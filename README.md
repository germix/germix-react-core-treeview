# Germix React Core - TreeView

## About

Germix react core treeview components

## Installation

```bash
npm install @germix/germix-react-core-treeview
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
