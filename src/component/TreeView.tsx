import React from 'react';
import TreeItem from './TreeItem';
import TreeSubItem from './TreeSubItem';

interface Props
{
    items?,
    disabled? : boolean,

    onItemClick(item),
    
    isItemSelected(item),
    getItemId(item),
    getItemLabel(item),
    getItemMenu(item),
    hasChildren(item),
    getItemChildren(item),

    fullrow? : boolean,
    tabIndex?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TreeView extends React.Component<Props,State>
{
    render()
    {
        let className = ['treeview'];
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        if(this.props.fullrow)
        {
            className.push('fullrow');
        }
        return (
<>
<div
    className={className.join(' ')}
    tabIndex={this.props.disabled ? undefined : this.props.tabIndex}
>
    { this.props.items.map((item) =>
    {
        if(!this.props.hasChildren(item))
            return this.renderItem(item, 0);
        else
            return this.renderSubItem(item, 0);
    })}
</div>
</>
        );
    }

    renderItem = (item, level) =>
    {
        return <TreeItem
            key={this.props.getItemId(item)}
            level={level}
            label={this.props.getItemLabel(item)}
            selected={this.props.isItemSelected(item)}
            disabled={this.props.disabled}
            menu={this.props.getItemMenu(item)}
            onClick={() =>
            {
                if(this.props.disabled)
                    return;
                this.props.onItemClick(item);
            }}
        />;
    }

    renderSubItem = (item, level) =>
    {
        return <TreeSubItem
            fullrow={this.props.fullrow}

            key={this.props.getItemId(item)}
            level={level}
            label={this.props.getItemLabel(item)}
            disabled={this.props.disabled}
            menu={this.props.getItemMenu(item)}
        >
            {
                this.props.getItemChildren(item).map((subitem) =>
                {
                    if(!this.props.hasChildren(subitem))
                        return this.renderItem(subitem, level+1);
                    else
                        return this.renderSubItem(subitem, level+1);
                })
            }
        </TreeSubItem>;
    }
};
export default TreeView;
