import React from 'react';

interface Props
{
    selected?,
    disabled?,
    label?,
    menu?,
    level?,
    onClick?(),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TreeItem extends React.Component<Props,State>
{
    render()
    {
        const style : any =
        {
            '--level': this.props.level,
        };
        let className = ['treeview-item'];
        if(this.props.selected)
        {
            className.push('selected');
        }
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div
    className={className.join(' ')}
    style={style}
    onClick={this.props.onClick}
>
    <div className="label">
        { this.props.label }
    </div>
    { this.props.menu &&
        <div className="treeview-item-action-button"
            onClick={(e) =>
            {
                e.stopPropagation();
            }}
        >
            { this.props.menu }
        </div>
    }
</div>
        );
    }
};
export default TreeItem;
