import React from 'react';

interface Props
{
    level,
    disabled,
    label,
    menu,
    fullrow,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TreeSubItem extends React.Component<Props,State>
{
    state =
    {
        isOpen : false,
        contentHeight: 0,
        isTransitioning: false,
    }
    content;
    intervalId;

    render()
    {
        const itemStyle : any =
        {
            '--level': this.props.level,
        }
        const iconStyle =
        {
            transform: (this.state.isOpen) ? 'rotate(90deg)' : ''
        }
        const contentStyle =
        {
            height: this.state.contentHeight,
            overflow: !this.state.isOpen||this.state.isTransitioning ? 'hidden' : ''
        }

        return (
<div
    className={'treeview-subitem'}
>

    { /* Item */ }
    <div
        style={itemStyle}
        className={'treeview-item' + (this.props.disabled ? ' disabled' : '')}
        onClick={() =>
        {
            if(this.props.disabled)
                return;
            if(!this.props.fullrow)
                return;
            this.expand();
        }}
    >
        <i className="icon-right-open" style={iconStyle}></i>
        <div className="label"
            onClick={() =>
            {
                if(this.props.disabled)
                    return;
                if(this.props.fullrow)
                    return;
                this.expand();
            }}
        >
            { this.props.label }
        </div>
        { this.props.menu &&
            <div className="treeview-item-action-button"
                onClick={(e) =>
                {
                    e.stopPropagation();
                }}
            >
                { this.props.menu }
            </div>
        }
    </div>

    { /* Content of sub items */}
    <div
        ref={(e) => this.content = e}
        className={"treeview-subitem-content"}
        style={contentStyle}
    >
        { this.props.children }
    </div>
</div>
        );
    }

    private expand = () =>
    {
        if(!this.state.isTransitioning)
        {
            this.setState({
                contentHeight: this.content.scrollHeight,
            });
            setTimeout(() =>
            {
                this.setState({
                    isOpen: !this.state.isOpen,
                    contentHeight: this.state.isOpen ? 0 : this.content.scrollHeight,
                    isTransitioning: true
                });
                this.intervalId = setInterval(() =>
                {
                    if(!this.state.isOpen)
                    {
                        if(parseInt(this.content.offsetHeight) === 0)
                        {
                            clearInterval(this.intervalId);
                            
                            this.setState({
                                isTransitioning: false
                            });
                        }
                    }
                    else
                    {
                        if(this.content.offsetHeight === this.content.scrollHeight)
                        {
                            clearInterval(this.intervalId);
                            
                            this.setState({
                                isTransitioning: false,
                                contentHeight: 'auto',
                                contentOpacity: 1,
                            });
                        }
                    }
                }, 10);
            }, 10);
        }
    }
};
export default TreeSubItem;
